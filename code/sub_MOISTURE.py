import paho.mqtt.client as mqtt
import mysql.connector
def on_connect(client, userdata, flags, rc):
    print("connected with result code " + str(rc))
    client.subscribe("Agriculture_Monitoring/soil_moisture_sensor")
def on_message(client, userdata, message):
    print("---> recived message")
    print(f"message = {message.payload.decode()}")
    data = message.payload.decode('utf-8')
 
    print("Received data:")
   
    print("moisture:", data)
   
    connection = mysql.connector.Connect(
    user = "root",
    password = "root",
    host = "localhost",
    database ="moisture_db"
    )
    query = f"insert into moisture_values (moisture) values({data});"
    cursor = connection.cursor()
    cursor.execute(query)
    connection.commit()
    connection.close()
    print("moisture is inserted to database")
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect('localhost', 1883, 60)
client.loop_forever()
