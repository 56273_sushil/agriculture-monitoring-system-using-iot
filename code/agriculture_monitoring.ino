
#include "DHT.h"
#include<PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h> 

const char *ssid = "WIFI_SSID";
const char *pass = "WIFI_PASSWORD";
const char *broker = "192.168.41.116";
const int port = 1883;

#define DHTPIN 2 
#define DHTTYPE DHT22 
#define LedPin D6 // Red LED
#define PirSensor_Pin D7 // Input for HC-SR501
 
LiquidCrystal_I2C lcd(0x27,16,2);

const char* server = "api.thingspeak.com";
String apiKey = "THINGSPEAK_API_WRITE_KEY"; 

DHT dht(DHTPIN, DHTTYPE);
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
WiFiClient client;
int pirValue = 0; 
void setup() {
   
              Serial.begin(9600);
              Serial.flush();
              delay(500);
              pinMode(LedPin, OUTPUT);
              pinMode(PirSensor_Pin, INPUT);
              Serial.println(F("test!"));
              lcd.begin();  
              lcd.backlight();
              dht.begin();
              digitalWrite(LedPin, LOW);
              Serial.println("Connecting to ");
              Serial.println(ssid);
              WiFi.begin(ssid, pass);
              while (WiFi.status() != WL_CONNECTED) 
              {
               delay(500);
               Serial.print(".");
              }
             Serial.println("");
             Serial.print("Connected to wifi. IP of NodeMCU is ");
             Serial.println(WiFi.localIP());
             mqttClient.setServer(broker, port);
}

void loop() {
   
             delay(2000);
             char message[15];
             float h = dht.readHumidity();
             float t = dht.readTemperature();
             float f = dht.readTemperature(true);
             if (isnan(h) || isnan(t) || isnan(f)) 
             {
              Serial.println(F("Failed to read from DHT sensor!"));
              return;
             }
             float hif = dht.computeHeatIndex(f, h);
             float hic = dht.computeHeatIndex(t, h, false);
             sprintf(message, "humidity=%.2f",h);
             lcd.setCursor(0,0);
             lcd.print(message);
             sprintf(message, "temperature=%.2f",t);
             lcd.setCursor(0,1);
             lcd.print(message);
             pirValue = digitalRead(PirSensor_Pin);
             Serial.print(F("pirvalue: "));
             Serial.println(pirValue);
             delay(200);
             if(pirValue == 1)
             {
               digitalWrite(LedPin, HIGH);
             }
             else
             {
               digitalWrite(LedPin, LOW);
             }
             int value=25;
             float moisture1 = analogRead(A0);
             float moisture=(100 - ( (moisture1/1024.00) * 100 ) );
             float m=moisture;
             Serial.println("moisture= " + String(moisture));
 
             if(moisture<value)
	           {
	        	   digitalWrite(D5,HIGH);
	           }
	           else
	           {
	        	   digitalWrite(D5,LOW);
	           }
             Serial.print(F("Humidity: "));
             Serial.print(h); 
             Serial.print(F("%  Temperature: "));
             Serial.print(t);
             Serial.print(F("°C "));
             Serial.print(f);
             Serial.print(F("°F  Heat index: "));
             Serial.print(hic);
             Serial.print(F("°C "));
             Serial.print(hif);
             Serial.println(F("°F"));
             if (client.connect(server,80))   //   "184.106.153.149" or api.thingspeak.com
              {  
                String postStr = apiKey;
                postStr +="&field1=";
                postStr += String(t);
                postStr +="&field2=";
                postStr += String(h);
                postStr +="&field3=";
                postStr += String(pirValue);
                postStr +="&field4=";
                postStr += String(m);
                postStr += "\r\n\r\n";
 
                client.print("POST /update HTTP/1.1\n");
                client.print("Host: api.thingspeak.com\n");
                client.print("Connection: close\n");
                client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
                client.print("Content-Type: application/x-www-form-urlencoded\n");
                client.print("Content-Length: ");
                client.print(postStr.length());
                client.print("\n\n");
                client.print(postStr);
                delay(200);
                   
 
                Serial.print("Temperature: ");
                Serial.print(t);
                Serial.print(" degrees Celcius, Humidity: ");
                Serial.print(h);
                Serial.println("%");
                Serial.print("moisture: ");
                Serial.print(m);
                Serial.println("%. Send to Thingspeak."); 
              }
             client.stop();
             if (!mqttClient.connected())
             {
               while (!mqttClient.connected())
                {
                  Serial.println(".");
                  mqttClient.connect("NodeMCU");
                  delay(500);
                }
               Serial.println("");    
               Serial.println("Connected to broker!!");
               delay(1000);
              }
              StaticJsonDocument<200> doc;
              doc["temperature"] = t;
              doc["humidity"] = h;
              char payload[200];
              serializeJson(doc, payload);
              mqttClient.publish("Agriculture_Monitoring/DHT_Sensor",payload);
             
              char message1[15];
              sprintf(message1, "moisture=%.2f",m); 
              mqttClient.publish("Agriculture_Monitoring/soil_moisture_sensor",message1);
              mqttClient.loop();
}
