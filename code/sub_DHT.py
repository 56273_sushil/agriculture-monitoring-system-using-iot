import paho.mqtt.client as mqtt
import mysql.connector
import json
def on_connect(client, userdata, flags, rc):
    print("connected with result code " + str(rc))
    client.subscribe("Agriculture_Monitoring/DHT_Sensor")
def on_message(client, userdata, message):
    print("---> recived message")
    print(f"message = {message.payload.decode()}")
    payload = message.payload.decode('utf-8')
    data = json.loads(payload)
    print("Received data:")
    print("Temperature:", data["temperature"])
    print("Humidity:", data["humidity"])
    value1=data["temperature"]
    value2=data["humidity"]
    connection = mysql.connector.Connect(
    user = "root",
    password = "root",
    host = "localhost",
    database ="moisture_db"
    )
    query = f"insert into dht_value (temperture,humidity) values({value1},{value2});"
    cursor = connection.cursor()
    cursor.execute(query)
    connection.commit()
    connection.close()
    print("temp and humidity is inserted to database")
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect('localhost', 1883, 60)
client.loop_forever()